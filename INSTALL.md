# Dev (conda)

## Create conda env for dev

```
git clone https://forgemia.inra.fr/philippe.bordron/rba.git
conda env create -f rba/conda.yml
```

## Init rba-online

```
cd rba
conda activate rba-online
python manage.py makemigrations
python manage.py migrate
```


## Start dev server

```
python manage.py runserver
```

# Prod (apache + venv)

Based on ubuntu 18.04

## Install requirements

```
sudo apt update
sudo apt upgrade -y

# Install python + pip + venv 
sudo apt install -y python3 python3-pip python3-venv

# Install system requirements for installing python modules
sudo apt install -y build-essential cmake libglpk-dev swig libgmp-dev
```

## Create `rba` user

First we create a `rba` user (along with `rba` group) that will run `rba-online`

```
sudo useradd -m -U rba
```

Then we clone rba-online
```
sudo -u rba git clone https://forgemia.inra.fr/philippe.bordron/rba.git /home/rba/rba
```

## Create rba venv

```
# Create venv
sudo python3 -m venv /usr/local/venv/rba-online

# Install python requirements
sudo /usr/local/venv/rba-online/bin/python3 -m pip install -U pip setuptools
sudo /usr/local/venv/rba-online/bin/python3 -m pip install -r /home/rba/rba/requirements.txt
```

The venv can be replaced by then conda installation. You just need to replace the venv path by the conda env path in the next sections


## Init `rba-online`

Finally, we init the project

```
# init
cd /home/rba/rba
sudo -u rba /usr/local/venv/rba-online/bin/python3 manage.py makemigrations
sudo -u rba /usr/local/venv/rba-online/bin/python3 manage.py migrate

# create super user
sudo -u rba /usr/local/venv/rba-online/bin/python3 manage.py createsuperuser
```

## Test in dev mode

We test the install in dev mode. Please, [replace "my-domain.tld" by your server ip/domain](https://docs.djangoproject.com/en/2.2/ref/settings/#allowed-hosts).

```
# We set the sites this Django site can serve. This is a security measure to prevent HTTP Host header attacks, which are possible even under many seemingly-safe web server configurations.
sudo -u rba sed -i.bak 's/^ALLOWED_HOSTS = .*$/ALLOWED_HOSTS = ["localhost", "my-domain.tld"]/' rba_online_simulator/settings.py

# We allow anyone to connect to the app
sudo -u rba /usr/local/venv/rba-online/bin/python3 manage.py runserver 0.0.0.0:8000
```

Connect to the server on port `8000` to look if page display correctly.
Jobs wont work as dev mode redirect on localhost, but it will work in production.

You really want to test it, you can do a ssh tunnel such as:

```
ssh my-server -L 8000:localhost:8000
```

and connect to http://localhost:8000

## Switch to prod with apache2

### API secret key

API secret key can be set by using environment variables or a dotenv (`.env`) file. We use the dotenv file strategy here. Editing the `/etc/apache2/envvars` also works, but atm using `SetEnv` for setting env variable in apache2 is problematic with rba-online.

We create an api secret key that we put in the `/home/rba/rba/.env` file which has the following format:

```
SECRET_KEY='my-secret-key'
```

This file can be generated with following command:

```
/usr/local/venv/rba-online/bin/python3 -c "from django.core.management.utils import get_random_secret_key; print('SECRET_KEY=\'' + get_random_secret_key() + '\'')" | sudo -u rba tee /home/rba/rba/.env
```

### Static files

We also generate static files (for CDN, but we will reuse them for apache)

```
sudo -u rba /usr/local/venv/rba-online/bin/python3 manage.py collectstatic --noinput
```

### Apache itself
We install apache + mod wsgi

```
sudo apt install -y apache2 apache2-utils libapache2-mod-wsgi-py3
```

We configure apache in a similar way to the one provided in [django documentation](https://docs.djangoproject.com/fr/2.2/howto/deployment/wsgi/modwsgi/)

- We add  `WSGIApplicationGroup %{GLOBAL}` directive in `/etc/apache2/apache2.conf` (We get a "Truncated or oversized response headers received from daemon process" error else)

```
sudo bash -c "echo 'WSGIApplicationGroup %{GLOBAL}' >> /etc/apache2/apache2.conf"
```

- We create `/etc/apache2/sites-available/rba-online.conf` with following content:

```
<VirtualHost *:80>

  Alias /robots.txt /home/rba/rba/static/robots.txt
  Alias /favicon.ico /home/rba/rba/static/favicon.ico

  Alias /media/ /home/rba/rba/media/
  Alias /static/ /home/rba/rba/static/
  Alias /rba/static/ /home/rba/rba/static/

  <Location /admin>
    Order deny,allow
    Deny from all
  </Location>

  <Directory /home/rba/rba/static>
    Require all granted
  </Directory>

  <Directory /home/rba/rba/media>
    Require all granted
  </Directory>

  <Directory /home/rba/rba/rba_online_simulator>
    <Files wsgi.py>
      Require all granted
    </Files>
  </Directory>

  WSGIDaemonProcess rba python-home=/usr/local/venv/rba-online python-path=/home/rba/rba user=rba group=rba
  WSGIProcessGroup rba
  WSGIScriptAlias / /home/rba/rba/rba_online_simulator/wsgi.py process-group=rba

</VirtualHost>
```

Finally, we apply the changes

```
# We check the syntax
sudo apache2ctl configtest

# We deactivate the default page
sudo a2dissite 000-default

# We activate rba-online
sudo a2ensite rba-online

# We reload apache2 config
sudo systemctl reload apache2
```

We can connect to the server to check if anything work correctly
